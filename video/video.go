package video

import (
	"fmt"
	"os/exec"

	"gitlab.com/emaele/telegram-videodl-bot/utility"
)

const big int64 = 10000

func ConvertWebm(path string) string {

	converted := fmt.Sprintf("%d.mp4", utility.RandomNumber(big))

	cmd := exec.Command("ffmpeg", "-i", path, converted)
	if _, err := cmd.CombinedOutput(); err == nil {
		/*log := string(stdoutStderr)
		fmt.Println(log)*/
	}

	return converted
}

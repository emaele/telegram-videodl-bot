package utility

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

//DownloadFile downloads a file using a GET http request
func DownloadFile(filepath string, url string) (err error) {

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer CloseSafely(out)

	// Get the data
	resp, err := http.Get(url) // nolint: gosec
	if err != nil {
		return err
	}
	defer CloseSafely(resp.Body)

	// Check server response
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %s", resp.Status)
	}

	// Writer the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

//CloseSafely closes an entity and logs in case of errors
func CloseSafely(toClose io.Closer) {
	err := toClose.Close()
	if err != nil {
		log.Println(err)
	}
}

package private

import (
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/emaele/telegram-videodl-bot/config"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/emaele/telegram-videodl-bot/utility"
	"gitlab.com/emaele/telegram-videodl-bot/video"
)

func HandleText(message *tgbotapi.Message, bot *tgbotapi.BotAPI, config config.Config) {
	var videoPath string

	links := utility.FindURLs(message.Text)
	for _, link := range links {

		if strings.HasSuffix(link, ".webm") {
			stamp := time.Now()
			convertAndSend(stamp.Format("Mon Jan 2 15:04:05 -0700 MST 2006"), link, message.Chat.ID, bot)
		} else {

			action := tgbotapi.NewChatAction(message.Chat.ID, "typing")
			bot.Send(action)

			fmt.Printf("%s sent a video: %s \n", utility.GetHandleOrUserName(message.From), links[0])
			if fileName, err := utility.GetVideo(link); err == nil {

				videoPath = config.DownloadDirectory + fileName

				// Sending Video status
				action := tgbotapi.NewChatAction(message.Chat.ID, "upload_video")
				bot.Send(action)

				msg := tgbotapi.NewVideoUpload(message.Chat.ID, videoPath)

				if _, err := bot.Send(msg); err != nil {
					msg := tgbotapi.NewMessage(message.Chat.ID, "Filesize is too big for Telegram (or bot.Send failed), download it at "+config.WebSiteURL+"video/"+fileName)
					bot.Send(msg)
				} else {
					os.Remove(videoPath)
				}
			} else if message.Chat.IsPrivate() {
				msg := tgbotapi.NewMessage(message.Chat.ID, "youtube-dl failed. This is so sad")
				bot.Send(msg)
			}
		}
	}
}

func HandleDocument(doc *tgbotapi.Document, id int64, bot *tgbotapi.BotAPI) {

	link, _ := bot.GetFileDirectURL(doc.FileID)
	if strings.HasSuffix(doc.FileName, ".webm") {

		convertAndSend(doc.FileID, link, id, bot)

	}

}

func convertAndSend(fileid string, link string, id int64, bot *tgbotapi.BotAPI) {

	filename := fileid + ".webm"

	utility.DownloadFile(filename, link)

	convertedPath := video.ConvertWebm(filename)

	os.Remove(filename)

	// Sending Video status
	action := tgbotapi.NewChatAction(id, "upload_video")
	bot.Send(action)

	_, err := bot.Send(tgbotapi.NewVideoUpload(id, convertedPath))
	if err == nil {
		os.Remove(convertedPath)
	}

}

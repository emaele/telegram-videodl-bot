package config

import (
	"errors"

	"github.com/BurntSushi/toml"
)

// Config is the bot configuration representation, read
// from a configuration file.
type Config struct {
	TelegramTokenBot  string
	DownloadDirectory string
	TempDirectory     string
	WebSiteURL        string
}

// ReadConfig loads the values from the config file
func ReadConfig(path string) (Config, error) {
	var conf Config

	if _, err := toml.DecodeFile(path, &conf); err != nil {
		return Config{}, err
	}

	if conf.TelegramTokenBot == "" {
		return newErr("missing Bot token")
	} else if conf.DownloadDirectory == "" {
		return newErr("missing download directory")
	} else if conf.TempDirectory == "" {
		return newErr("missing temp download directory")
	} else if conf.WebSiteURL == "" {
		return newErr("missing website url")
	}

	return conf, nil
}

func newErr(message string) (Config, error) {
	return Config{}, errors.New(message)
}

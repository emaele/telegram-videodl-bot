package main

import (
	"flag"
	"fmt"
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	conf "gitlab.com/emaele/telegram-videodl-bot/config"
	"gitlab.com/emaele/telegram-videodl-bot/private"
)

var (
	config         conf.Config
	err            error
	debug          bool
	configFilePath string
)

func main() {

	setCLIParams()

	config, err = conf.ReadConfig(configFilePath)
	if err != nil {
		log.Fatal(err)
	}

	bot, err := tgbotapi.NewBotAPI(config.TelegramTokenBot)
	if err != nil {
		log.Fatal(err)
	}

	bot.Debug = debug

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)
	if err != nil {
		log.Fatal(err)
	}

	for update := range updates {
		if update.Message != nil {
			go mainBot(bot, update.Message)
		}
	}

}

func mainBot(bot *tgbotapi.BotAPI, message *tgbotapi.Message) {
	if message.Chat.IsPrivate() {
		if message.Text != "" {
			private.HandleText(message, bot, config)
		} else if message.Document != nil {
			private.HandleDocument(message.Document, message.Chat.ID, bot)
		}
	}
}

func setCLIParams() {
	fmt.Println(err)
	flag.BoolVar(&debug, "debug", false, "activate all the debug features")
	flag.StringVar(&configFilePath, "config", "./config.toml", "configuration file path")
	flag.Parse()
}
